from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers import LSTM
from keras.layers.normalization import BatchNormalization
from keras.optimizers import SGD,RMSprop,adam
from keras.utils import np_utils
from keras import losses

import numpy as np
import os
from PIL import Image
from numpy import *

from sklearn.cross_validation import train_test_split

img_rows, img_cols = 78, 78
img_channels = 1

#path2='..\..\PythonApplication2\PythonApplication2\preprocessed2'
path2 = ".\input_data"
imlist = os.listdir(path2)

print("Data is loading. Please, wait...")
frames = []
for im2 in imlist:
    im = Image.open(path2+ '\\' + im2).convert('L')
    
    tv = [(255 - x) * 1.0 / 255.0 for x in im.getdata()]
    tva = np.reshape(tv, (img_rows, img_cols))
   # tva = tva.transpose()
    frames.append(tva)
frames = np.array(frames)

liss=np.genfromtxt("HWHLs", usecols=[0], unpack=True)
lis=liss[0:5000]

#x = np.array([ frames[0] for x in frames ])
#y = np.array([ lis[0] for x in lis ])

#data,y = shuffle(frames, liss, random_state=2)
#(X, Y) = [data, y]

X_train, X_test, y_train, y_test = train_test_split(frames, lis, test_size=0.2, random_state=4)
X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols)
X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
y_train = y_train.astype('float32')
y_test = y_test.astype('float32')

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')
y_size=y_train.shape[0]

model = Sequential()
from keras.layers.convolutional import Conv2D, MaxPooling2D

model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 data_format='channels_first',
                 input_shape=(img_channels, img_rows, img_cols)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(3, 3)))
model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(Dense(232, activation='softmax'))
model.add(Dropout(0.2))
model.add(Dense(1, activation='linear'))

SGD(lr=0.001, momentum=0.0, decay=0.0, nesterov=False)
model.compile(loss=losses.mse, optimizer='adadelta')

model.fit(X_train, y_train, batch_size=32, epochs=50, verbose=1)

score = model.evaluate(X_test, y_test, verbose=0)
print('Test loss:', score)
print(model.summary())
print(model.predict(X_train[0:3]))
print(y_train[0:3])
