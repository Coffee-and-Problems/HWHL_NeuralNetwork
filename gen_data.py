# Создание данных для обучения


from matplotlib.pyplot import savefig
from math import exp, pi, sqrt, log
import matplotlib.pylab as plt
import numpy as np
import random
hwhls=[]
sigmas=[]
#сделаем 2 картинки с данными
for i in range(20000):
#параметры нормального распределения
    mu=0
    #random.uniform(-20,20)
    sigma=random.uniform(0,40)
    x = np.arange(-100, 100, 0.1)
    f = (1/(sigma*sqrt(2*pi)))*np.exp(-(x-mu)**2/(2*sigma**2))

    #построение гауссиан в png
    path1=".\input_data"

    plt.figure(str(i), figsize=(5/2.54,5/2.54))
   # plt.axis('off')
    plt.axis([-100.0, 100.0, 0, 0.05])
    plt.axis('off')
    plt.plot(x, f, lw=4.0)
    savefig(path1 + '\\' + str(i)+'.png', dpi=40)
    plt.clf()

    #полуширина и дисперсия гауссианки и список верных результатов
    hwhl=sqrt(2*log(2))*sigma
    sigmas.append(sigma)
    hwhls.append(hwhl)
    np.savetxt("sigmas.dat", sigmas)
    np.savetxt("HWHLs", hwhls)
